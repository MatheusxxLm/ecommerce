﻿using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;

namespace Ecommerce.Auth
{
    public class AuthService
    {
        private readonly IConfiguration _configuration;
        public AuthService(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public string GenerateJwtToken(string email)
        {
            var key = _configuration["JwtConfig:Key"];
            var issuer = _configuration["JwtConfig:Issuer"];
            var audience = _configuration["JwtConfig:Audience"];

            var sec = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(key));
            var credentials = new SigningCredentials(sec, SecurityAlgorithms.HmacSha256);

            var claims = new List<Claim>
            {
               new Claim("UserName", email)
               
            };

            var token = new JwtSecurityToken(
                issuer: issuer,
                audience: audience,
                signingCredentials: credentials,
                expires: DateTime.Now.AddMinutes(120),
                claims: claims
            );

            var tokenHandler = new JwtSecurityTokenHandler();
            var stringToken = tokenHandler.WriteToken(token);
            return stringToken;
        }
 
        public string GenerateHash(string senha)
        {
            using (var sha256 = SHA256.Create())
            {
                var bytesSenha = Encoding.UTF8.GetBytes(senha);
                var hash = sha256.ComputeHash(bytesSenha);
                var hashString = Convert.ToHexString(hash);

                return hashString;
            }
        }
    }
}
