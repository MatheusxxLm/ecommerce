﻿using Ecommerce.Auth;
using Ecommerce.Data.Models;
using Ecommerce.Repository.Interfaces.Auth;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SendEmail;
using System.Globalization;

namespace Ecommerce.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AutenticacaoController : Controller
    {
        private readonly IConfiguration _config;
        private readonly AuthService _authService;
        private readonly IUsuarioRepository _usuarioRepository;
        public AutenticacaoController(IConfiguration Configuration, IUsuarioRepository usuarioRepository, AuthService authService)
        {
            _config = Configuration;
            _usuarioRepository = usuarioRepository;
            _authService = authService;

        }
        [HttpPost("CriarUsuario")]
        public async Task<IActionResult> CriarUsuario(Usuario usuario)
        {
            var hash = _authService.GenerateHash(usuario.SenhaHash);
            usuario.SenhaHash = hash;
            await _usuarioRepository.CriarUsuarioAsync(usuario);

            

            return Ok(usuario);
        }

        [HttpPost("Login")]
        public async Task<IActionResult> Login(Usuario usuario)
        {
            // var verificarUsuario = await _usuarioRepository.ObterUsuarioPorEmailAsync(usuario.Email);
            // Aplicar validações padronizadas.
       
            var tokenString = _authService.GenerateJwtToken(usuario.Email);
            return Ok(new { token = tokenString });
        }

        [HttpGet("TestarRequisicao")]
        [Authorize]
        public  async Task<IActionResult> TestarToken(string seila)
        {
            return Ok("sdasdasdasds");
        }
    }
}
