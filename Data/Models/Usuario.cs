﻿using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Security.Cryptography;

namespace Ecommerce.Data.Models
{

    public class Usuario
    {
        [Key] public int IdUsuario { get; set; }
        public required string Nome { get; set; }
        public required string Email { get; set; }
        public required string CPF { get; set; }
        public required string SenhaHash { get; set; }

        public static implicit operator string(Usuario? usuario) => usuario.ToString() ?? string.Empty;

        //public bool Autenticar(string senhaDigitada)
        //{
        //    var hashDigitado = GerarHash(senhaDigitada);
        //    return hashDigitado == this.SenhaHash;
        //}
    }
}
