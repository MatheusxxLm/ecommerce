﻿using Ecommerce.Data;
using Ecommerce.Data.Models;

using Ecommerce.Repository.Interfaces.Auth;
using Microsoft.EntityFrameworkCore;
using System;

namespace Ecommerce.Repository.Auth
{
    public class UsuarioRepository : IUsuarioRepository
    {
        private readonly EcommerceContext _context;
        public UsuarioRepository(EcommerceContext context) : base()
        {
            _context = context;
        }

        public async Task<Usuario> CriarUsuarioAsync(Usuario usuario)
        {
            _context.Usuarios.Add(usuario);
            await _context.SaveChangesAsync();
            return usuario;
        }

        public void Atualizar(Usuario entity)
        {
            throw new NotImplementedException();
        }

        public void Deletar(Usuario entity)
        {
            throw new NotImplementedException();
        }

        public async Task<string> ObterUsuarioPorEmailAsync(string email)
        {
            return await _context.Usuarios
                .Where(u => u.Email == email)
                .Select(x => x.Email)
                .FirstOrDefaultAsync();
        }
    }
}
