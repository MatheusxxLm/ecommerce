﻿using Ecommerce.Data.Models;

namespace Ecommerce.Repository.Interfaces.Auth
{
    public interface IUsuarioRepository
    {
        Task<Usuario> CriarUsuarioAsync(Usuario usuario);
        Task<string> ObterUsuarioPorEmailAsync(string email);
    }
}
