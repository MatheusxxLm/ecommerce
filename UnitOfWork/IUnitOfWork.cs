﻿using Ecommerce.Repository.Auth;
using Ecommerce.Repository.Interfaces.Auth;

namespace Ecommerce.UnitOfWork
{
    public interface IUnitOfWork
    {
        IUsuarioRepository UsuarioRepository { get; }
        void Begin();
        void Commit();
        void Rollback();
        void Dispose();
    }
}
